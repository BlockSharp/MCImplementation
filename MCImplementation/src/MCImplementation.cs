﻿using System;
using System.Collections.Generic;
using BlockCSharp;
using BlockCSharp.ModLoader;
using BlockCSharp.Vector;
using BlockCSharp.World;

namespace MCImplementation
{
    public class MCImplementation : Mod
    {
        public MCImplementation()
        {
            InternalName = "mc_implementation";
            DisplayName = "MCImplementation";
            Version = new Version(0, 0, 0, 0);
            
            Dependencies = new List<Dependency>();
        }
        
        public override void PreInit()
        {
            Console.WriteLine("PreInit!");
        }

        public override void Init()
        {
            Console.WriteLine("Init!");
        }

        public override void PostInit()
        {
            Console.WriteLine("PostInit!");
            
            World.AddChunk(new Vector2i(0, 0));
            
            World.SetBlock(new Vector3i(0, 1, 0), Registries.BlockRegistry.Get(new RegistryKey("minecraft:dirt")));
            World.SetBlock(new Vector3i(1, 1, 0), Registries.BlockRegistry.Get(new RegistryKey("blocksharp:test_block")));
        }
    }
}
