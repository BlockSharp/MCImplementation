﻿using BlockCSharp;
using BlockCSharp.Attributes;
using BlockCSharp.BaseClasses;
using BlockCSharp.Interfaces;
using BlockCSharp.Vector;
using BlockCSharp.World;

namespace MCImplementation.Blocks
{
    [Block(SRegistryKey = "minecraft:dirt")]
    public class Dirt : Block, IBlockModel
    {
        public Model Model { get; set; }

        public override void Start(Vector3i blockPosition)
        {
            Position = blockPosition;
            Opaque = true;
            
            Update(this);
            World.DispatchBlockUpdate(this);
        }

        public override void Update(Block updater)
        {
            Model = PredefinedModels.SimpleCubeModel(Position, World.OpaqueSurroundings(Position));

            if (!World.ChunkUpdateQueue.Contains(World.GetChunk(World.BlockToChunkPosition(Position)))
            )
                World.ChunkUpdateQueue.Push(World.GetChunk(World.BlockToChunkPosition(Position)));

        }
    }
}